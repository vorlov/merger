```
usage: merge.py [-h] [--dir DIR] [--to_file TO_FILE] [--from_type FROM_TYPE] [--to_type TO_TYPE] [--month_year MONTH_YEAR]

optional arguments:
  -h, --help            show this help message and exit
  --dir DIR             Directory where stores xls|xlsx|csv files
  --to_file TO_FILE     Path to output file
  --from_type FROM_TYPE
                        From which type of files will be parsed: 1 - xls, 2 - xlsx, 3 - csv, 4 - html
  --to_type TO_TYPE     From which type of file will be wrote: 1 - xls, 2 - xlsx, 3 - csv, 4 - html
  --month_year MONTH_YEAR
                        Filter files by month and year. Format MMM-yyyy
```