import pandas as pd
import argparse
import typing
import subprocess
import os

from tqdm import tqdm
from pathlib import Path
from datetime import date
from xlrd.biffh import XLRDError

FILE_EXT = {
    '1': 'xls',
    '2': 'xlsx',
    '3': 'csv',
    '4': 'html'
}


def find_and_filter_by_month(
        ext: str,
        source_dir: str,
        month_year: str
) -> typing.List[str]:
    _command = f'find {source_dir} -maxdepth 1 -newermt "01-{month_year} -1 sec" ' \
               f'-and -not -newermt "01-{month_year} +1 month -1 sec" -name "*.{ext}"'

    _output = subprocess.check_output(
        _command,
        shell=True
    ).decode().split('\n')
    return _output[:(len(_output) - 1)]


def sort_by_date(files: typing.List[str]):
    files.sort(key=os.path.getmtime)
    return files


def files_path_to_frames(files: typing.List[str], sys_args) -> typing.List[pd.DataFrame]:
    frames = []
    for i, name in enumerate(files):
        if sys_args.from_type in ['1', '2']:
            try:
                frames.append((pd.read_excel(name, index_col=None)).head(-1))  # .head(-1) for drop last row
            except XLRDError:
                frames.append((pd.read_html(name, index_col=None)[0]).head(-1))
        elif sys_args.from_type == '3':
            frames.append((pd.read_csv(name, index_col=None)).head(-1))
        elif sys_args.from_type == '4':
            frames.append((pd.read_html(name, index_col=None)[0]).head(-1))
    return frames


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--dir", help='Directory where stores xls|xlsx|csv files', default='.')
    parser.add_argument("--to_file", help='Path to output file', default=date.today().strftime("%d-%m-%Y") + '.xlsx')
    parser.add_argument("--from_type", help='From which type of files will be parsed:\n'
                                            '1 - xls,\n'
                                            '2 - xlsx,\n'
                                            '3 - csv,\n'
                                            '4 - html')
    parser.add_argument("--to_type", help='From which type of file will be wrote:\n'
                                          '1 - xls,\n'
                                          '2 - xlsx,\n'
                                          '3 - csv,\n'
                                          '4 - html')
    parser.add_argument("--month_year", help='Filter files by month and year. Format MMM-yyyy')

    args = parser.parse_args()

    with tqdm(total=100,
              bar_format='{desc}',
              mininterval=0.05) as pbar:
        pbar.set_description(f'Finding files...')
        files = sort_by_date(find_and_filter_by_month(FILE_EXT[args.from_type], args.dir, args.month_year))
        if not files:
            raise Exception('No matching files')
        pbar.set_description(f'Converting files...')
        frames = files_path_to_frames(files, args)
        pbar.set_description('Merging files...')
        # Drop last row
        combined = pd.concat(frames)
        pbar.set_description(f'Writing result to {args.to_file}...')

        # Write to MMM-yyyy/args.to_file
        dir_path = Path(args.dir) / args.month_year
        dir_path.mkdir(exist_ok=True)
        file_path = dir_path / args.to_file
        if args.to_type in ['1', '2']:
            combined.to_excel(file_path, index=False)
        elif args.to_type == '3':
            combined.to_csv(file_path, index=False)
        elif args.to_type in '4':
            combined.to_html(file_path, index=False)

        pbar.set_description(f'Replacing files to {args.month_year}...')
        # Replace files to MMM-yyyy
        for file in files:
            Path(file).replace(dir_path / Path(file).name)
        pbar.set_description(f'Done')



if __name__ == '__main__':
    main()
